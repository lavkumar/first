'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Categories extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  Categories.init({
    Name: DataTypes.STRING,
    description: DataTypes.STRING,
    GroupsId: DataTypes.INTEGER,
    isactive: DataTypes.BOOLEAN
  }, {
    sequelize,
    modelName: 'Categories',
  });
  Categories.associate = function (models) {
    Categories.belongsTo(models.Groups);
  }
  Categories.associate = function (models) {
    Categories.hasMany(models.Products);
  }

  return Categories;
};