'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
     return queryInterface.bulkInsert('Groups', [{
      Name: 'group-1: kashmir articles',
      description: 'famous for kashmir articles',
      isactive: true,
      createdAt: new Date(),
      updatedAt: new Date(),
    },
    {
      Name: ' group-2:Telangana articles',
      description: 'famous for Telangana articles',
      isactive: true,
      createdAt: new Date(),
      updatedAt: new Date(),

    }, {
      Name: ' group-3:kerala articles',
      description: 'famous for kerala articles',
      isactive: true,
      createdAt: new Date(),
      updatedAt: new Date(),
    }], {});
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
     return queryInterface.bulkDelete('Groups', null, {});
  }
};
