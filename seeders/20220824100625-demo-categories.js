'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
     return queryInterface.bulkInsert('Categories', [{
      Name: 'Categories-1',
      description: 'famous for kashmir articles',
      GroupsId:1,
      isactive: true,
      createdAt: new Date(),
      updatedAt: new Date(),
    },
    {
      Name: ' Catogories-2:',
      description: 'famous for Telangana articles',
      GroupsId:2,
      isactive: true,
      createdAt: new Date(),
      updatedAt: new Date(),

    }, {
      Name: ' Catogeries-3',
      description: 'famous for kerala articles',
      GroupsId:3,
      isactive: true,
      createdAt: new Date(),
      updatedAt: new Date(),
    },{
      Name: ' Categories-3:',
      description: 'famous for kerala articles',
      GroupsId:4,
      isactive: true,
      createdAt: new Date(),
      updatedAt: new Date(),
    }], {});
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
     return queryInterface.bulkDelete('Categories', null, {});
  }
};
