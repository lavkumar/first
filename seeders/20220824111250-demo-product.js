'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
     await queryInterface.bulkInsert('Products', [{
      Name: "product1" ,
       description: "products1 Description", 
       image_url: "https://image1", 
       CategoryId: 1,
       isactive: true,
       createdAt:new Date(),
       updatedAt:new Date()

    },
  {
    Name: "product2" ,
    description: " Description", 
    image_url: "https://image2", 
    CategoryId: 1,
    isactive: true,
    createdAt:new Date(),
    updatedAt:new Date()
  },
  {
    Name: "product3" ,
    description: "Description", 
    image_url: "https://image3", 
    CategoryId: 2,
    isactive: true,
    createdAt:new Date(),
    updatedAt:new Date()
  },
  {
    Name: "product4" ,
       description: " Description", 
       image_url: "https://image4", 
       CategoryId: 2,
       isactive: true,
       createdAt:new Date(),
       updatedAt:new Date()

  }
])

  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
     await queryInterface.bulkDelete('Products', null, {});
  }
};
